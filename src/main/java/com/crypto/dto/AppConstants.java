package com.crypto.dto;

public interface AppConstants {
	
	String BASE_URL = "https://marketdata.tradermade.com/api/v1";
	
	String RANGE_ENDPOINT = BASE_URL + "/timeseries";
	
}
