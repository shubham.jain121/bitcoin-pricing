package com.crypto.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponseModel {

	private List<DatePricePair<String, String>> datePriceList = new ArrayList<>();
	
	private String currency;
	
	private String highest_price_date;
	
	private String highest_price;
	
	private String lowest_price_date;
	
	private String lowest_price;
	
	private String error;

}
