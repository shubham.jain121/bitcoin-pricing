package com.crypto.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DatePricePair<K, V> implements Comparable<K> {
	private K date;
	private V price;

	@Override
	public int compareTo(K o) {
		return this.compareTo(o);
	}
	
}