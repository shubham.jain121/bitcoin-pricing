package com.crypto.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoField;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crypto.caching.module.LRUCache;
import com.crypto.dto.AppConstants;
import com.crypto.dto.DatePricePair;
import com.crypto.dto.ResponseModel;

@Component
@Scope("singleton")
public class FetchData {

	LRUCache<String, String> cache = new LRUCache<String, String>(1000);

	@Value("${app.marketdata_tradermade_key}")
	private String marketdata_tradermade_key;
	
	public ResponseModel fetch(LocalDate start, LocalDate end, String currency)
			throws MalformedURLException, IOException, ParseException {

		ResponseModel responseModel = new ResponseModel();
		String api_key = marketdata_tradermade_key;
		String uri = String.format("%s?currency=%s&api_key=%s&start_date=%s&end_date=%s&format=%s",
				AppConstants.RANGE_ENDPOINT, "BTC" + currency.toUpperCase(), api_key, start.minusDays(1), end,
				"records");
		URL url;
		try {
			url = new URL(uri);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			responseModel.setError(e.getMessage());
//			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
			throw e;
		}
		HttpURLConnection connection = null;
		BufferedReader responseReader = null;
		String line;
		final StringBuilder res = new StringBuilder("");
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setReadTimeout(0);
			responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = responseReader.readLine()) != null) {
				res.append(line).append("\n");
			}
		} catch (IOException ioException) {
//        	return new ResponseEntity<>(ioException.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			responseModel.setError(ioException.getMessage());
			throw ioException;
		}

		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(res.toString());
		JSONArray jsonArray = (JSONArray) json.get("quotes");
		for (Object obj : jsonArray) {
			JSONObject tempJsonObj = (JSONObject) obj;
			String date = tempJsonObj.get("date").toString();
			String price = tempJsonObj.get("close").toString();
			cache.put(date + "_" + currency, price);
			responseModel.getDatePriceList().add(new DatePricePair<String, String>(date, price));
		}
		
		return responseModel;
	}

	public ResponseModel fetchFromCache(LocalDate start, LocalDate end, String currency) {
		ResponseModel responseModel = new ResponseModel();

		if (start.compareTo(end) > 0) {
			return responseModel;
		}

		while (start.compareTo(end) <= 0) {
			if (!isWeekend(start) && 
					cache.get(start.toString() + "_" + currency).isPresent()) {
				responseModel.getDatePriceList().add(new DatePricePair<String, String>(start.toString(),
						cache.get(start.toString() + "_" + currency).get()));
			}
			start = start.plusDays(1);
		}

		return responseModel;

	}

	private static boolean isWeekend(LocalDate localDate) {

		return (localDate.get(ChronoField.DAY_OF_WEEK) == 6) || (localDate.get(ChronoField.DAY_OF_WEEK) == 7);
	}

}
