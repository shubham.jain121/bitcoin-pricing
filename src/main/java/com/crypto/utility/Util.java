package com.crypto.utility;

import java.util.List;

import com.crypto.dto.DatePricePair;
import com.crypto.dto.ResponseModel;

public class Util {

	public void updateHighAndLowPrice(ResponseModel rm) {
		List<DatePricePair<String, String>> datePriceList = rm.getDatePriceList();
		Double highest_price = Double.MIN_VALUE;
		Double lowest_price = Double.MAX_VALUE;
		String highest_price_date = "";
		String lowest_price_date = "";
		for(DatePricePair<String, String> dp : datePriceList) {
			Double price = Double.parseDouble(dp.getPrice());
			if(highest_price < price) {
				highest_price = price;
				highest_price_date = dp.getDate();
			}
			if(lowest_price > price) {
				lowest_price = price;
				lowest_price_date = dp.getDate();
			}
		}
		
		rm.setHighest_price_date(highest_price_date);
		rm.setLowest_price_date(lowest_price_date);
		rm.setHighest_price(highest_price.toString());
		rm.setLowest_price(lowest_price.toString());
	}
}
