package com.crypto.exception;

import org.springframework.http.HttpStatus;

public class PriceException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private final HttpStatus status;
	
	public PriceException(String errorMsg, HttpStatus status) {
		super(errorMsg);
		this.status = status;
	}
	
	public HttpStatus getStatus() {
		return status;
	}

}
