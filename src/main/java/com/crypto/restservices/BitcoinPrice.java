package com.crypto.restservices;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crypto.dto.ResponseModel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/bitcoin")
public interface BitcoinPrice {
	
	@Operation(summary = "Get historical bitcoin price in input currency for the input date range")
	@ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Got the bitcoin price", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseModel.class))}),
			@ApiResponse(responseCode = "429", description = "Request limit reached", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error", content = @Content)})
	@GetMapping("/price")
	public ResponseEntity<ResponseModel> getBitCoinPrice(@RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
			@RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate, @RequestParam("currency") String currency);
	
}