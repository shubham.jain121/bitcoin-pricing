package com.crypto.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.time.Duration;
import java.time.LocalDate;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.crypto.dto.ResponseModel;
import com.crypto.restservices.BitcoinPrice;
import com.crypto.utility.FetchData;
import com.crypto.utility.Util;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BitcoinPriceService implements BitcoinPrice {

	private final Bucket bucket;

	public BitcoinPriceService() {
		Bandwidth limit = Bandwidth.classic(2, Refill.greedy(20, Duration.ofMinutes(1)));
		this.bucket = Bucket.builder().addLimit(limit).build();
	}

	@Autowired
	FetchData fetchData;

	@Override
	public ResponseEntity<ResponseModel> getBitCoinPrice(LocalDate from, LocalDate to, String currency) {
		if (bucket.tryConsume(1)) {
			log.info("Entering getPrice with from: {} and to: {} & currency: {}", from, to, currency);
			ResponseModel response = new ResponseModel();
			try {
				response = fetchData.fetch(from, to, currency);
				response.setCurrency(currency);

			} catch (MalformedURLException e) {
				e.printStackTrace();
				response.setError(e.getMessage());
				// return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

			} catch (IOException ioException) {
				response.setError(ioException.getMessage());
				// return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

			} catch (ParseException pe) {
				response.setError(pe.getMessage());
				// return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (Exception e) {
				response.setError("Generic exception " + e.getMessage());
				// return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			try {
				if (response.getError() != null) {
					response = fetchData.fetchFromCache(from, to, currency);
				}
				response.setCurrency(currency);
				response.setError(null);
			} catch (Exception e) {
				response.setError("Generic exception while fetching from cache" + e.getMessage());
				return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			new Util().updateHighAndLowPrice(response);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
	}

}
